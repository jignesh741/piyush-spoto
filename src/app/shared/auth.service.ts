import { Injectable } from '@angular/core';
import { from, of } from 'rxjs';

import { getUserRole } from 'src/app/utils/util';

export interface ISignInCredentials {
  email: string;
  password: string;
}

export interface ICreateCredentials {
  email: string;
  password: string;
  displayName: string;
}

export interface IPasswordReset {
  code: string;
  newPassword: string;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor() {}

  // tslint:disable-next-line:typedef
  signIn(credentials: ISignInCredentials) {
    return of({});
  }

  // tslint:disable-next-line:typedef
  signOut() {
    return true;
  }

  // tslint:disable-next-line:typedef
  register(credentials: ICreateCredentials) {
    
  }

  // tslint:disable-next-line:typedef
  sendPasswordEmail(email) {
   
  }

  // tslint:disable-next-line:typedef
  resetPassword(credentials: IPasswordReset) {
   
  }

  // tslint:disable-next-line:typedef
  async getUser() {
    const u = await of({displayName : 'abc'}) //this.auth.currentUser;
    return { ...u, role: getUserRole() };
  }
}
