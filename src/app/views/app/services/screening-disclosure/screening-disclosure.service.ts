// Angular
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

// Rxjs
import { Observable, of } from 'rxjs';
import { API } from 'src/app/constants/api-name.constant';
import { environment } from 'src/environments/environment';

// Constants

@Injectable({
    providedIn: 'root'
})
export class ScreeningDisClosureService {

    constructor(private http: HttpClient) { }

    getAuthorisation(): Observable<any> {
        return of(
            {
                "status": 200,
                "message": {
                    "en": "OK",
                    "hin": "OK"
                },
                "data": {
                    authoriseText: 'In addition to the disclosure, employers must provide an authorization document. It is illegal to run a background check on any person, including criminal or social media searches, without first obtaining their authorization Like a disclosure, the authorization must be a standalone document. It should request consent for a background check, include contact information for the CRA that would compile the report and any relevant state laws. Beyond that, it may not contain any additional content.'
                }
            }
        )

        return this.http.get(environment.apiUrl + API.GET_AUTHRIZATION_TEXT);
    }

    getQuickTips(): Observable<any> {
        return of(
            {
                "status": 200,
                "message": {
                    "en": "OK",
                    "hin": "OK"
                },
                "data": [{
                    question: 'Lighting and exposure on face and background',
                    answer: `Anim pariatur cliche reprehenderit, enim eiusmod highlife accusamus terry richardson ad squid. 3 wolf moonofficia aute, non cupidatat skateboard dolor brunch.
                     Brunch 3wolf moon tempor, sunt aliqua put a bird on it squidsingle- origin coffee nulla assumenda shoreditch et.Nihil anim keffiyeh helvetica, craft beer labore wesanderson cred nesciunt sapiente ea proident.
                     Ad veganexcepteur butcher vice lomo.Leggings occaecat craftbeer farm - to - table, raw denim aesthetic synth nesciuntyou probably haven\'t heard of them accusamus laboresustainable VHS.`,
                    id: 'q1'
                },
                {
                    question: 'Head & Eyes - Position and Background',
                    answer: `Anim pariatur cliche reprehenderit, enim eiusmod highlife accusamus terry richardson ad squid. 3 wolf moonofficia aute, non cupidatat skateboard dolor brunch.Food truck quinoa nesciunt laborum eiusmod.
                     Brunch 3wolf moon tempor, sunt aliqua put a bird on it squidsingle-origin coffee nulla assumenda shoreditch et.Nihil anim keffiyeh helvetica, craft beer labore wesanderson cred nesciunt sapiente ea proident.`,
                    id: 'q2'
                },
                {
                    question: 'Head Converings',
                    answer: `Brunch 3wolf moon tempor, sunt aliqua put a bird on it squidsingle-origin coffee nulla assumenda shoreditch et.Nihil anim keffiyeh helvetica, craft beer labore wesanderson cred nesciunt sapiente ea proident.
                     Ad veganexcepteur butcher vice lomo. Leggings occaecat craftbeer farm-to-table, raw denim aesthetic synth nesciuntyou probably haven\'t heard of them accusamus laboresustainable VHS.`,
                    id: 'q3'
                },
                {
                    question: 'Eyeglasses',
                    answer: `Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.
                     Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.`,
                    id: 'q4'
                },
                {
                    question: 'Facial Experssion',
                    answer: `Sed volutpat mollis dui eget fringilla. Vestibulum blandit urna ut tellus lobortis tristique. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque quis cursus mauris.`,
                    id: 'q5'
                },
                {
                    question: 'Be the only person in the picture',
                    answer: `Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
                     Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.`,
                    id: 'q6'
                }]
            }
        )

        return this.http.get(environment.apiUrl + API.GET_QUICK_TIPS);
    }
}
