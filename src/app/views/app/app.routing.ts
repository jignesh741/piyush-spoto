import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [
            {
                path: 'screening-disclosure',
                loadChildren: () => import('./application/screening-disclosure/screening-disclosure.module')
                    .then(mod => mod.ScreeningDisclosureModule),
            },
            { path: '', pathMatch: 'full', redirectTo: 'screening-disclosure' },
            { path: '**', redirectTo: 'screening-disclosure' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
