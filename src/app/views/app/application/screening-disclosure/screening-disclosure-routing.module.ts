import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DigitalSignatureComponent } from './components/digital-signature/digital-signature.component';
import { PhotographTipsComponent } from './components/photograph-tips/photograph-tips.component';
import { ProfileScreeningComponent } from './components/profile-screening/profile-screening.component';
import { ScreeningDisclosureComponent } from './screening-disclosure.component';



const routes: Routes = [
  { path: '', component: ScreeningDisclosureComponent },
  { path: 'digital-signature', component: DigitalSignatureComponent },
  { path: 'profile-screening', component: ProfileScreeningComponent },
  { path: 'photogarph-tips', component: PhotographTipsComponent },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScreeningDisclosureRoutingModule { }
