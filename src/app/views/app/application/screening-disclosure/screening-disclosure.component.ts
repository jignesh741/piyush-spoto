import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ScreeningDisClosureService } from '../../services/screening-disclosure/screening-disclosure.service';

@Component({
  selector: 'app-screening-disclosure',
  templateUrl: './screening-disclosure.component.html',
  styleUrls: ['./screening-disclosure.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class ScreeningDisclosureComponent implements OnInit {

  authoriseDetail;

  constructor(
    private screeningDisClosureService: ScreeningDisClosureService,
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.getAuthorisation();
  }

  getAuthorisation() {
    this.screeningDisClosureService.getAuthorisation().subscribe(res => {
      if (res && res.data) {
        this.authoriseDetail = res.data;
      }
    })
  }

  onAuthoriseClick() {
    this.router.navigate(['app/screening-disclosure/photogarph-tips']);
  }
}
