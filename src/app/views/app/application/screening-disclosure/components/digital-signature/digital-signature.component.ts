import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ScreeningDisClosureService } from 'src/app/views/app/services/screening-disclosure/screening-disclosure.service';

@Component({
  selector: 'app-digital-signature',
  templateUrl: './digital-signature.component.html',
  styleUrls: ['./digital-signature.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class DigitalSignatureComponent implements OnInit {
  tipList = [];

  constructor(
    private screeningDisClosureService: ScreeningDisClosureService,
    public router: Router) { }

  ngOnInit(): void {
    this.getQuicktips();
  }

  getQuicktips() {
    this.screeningDisClosureService.getQuickTips().subscribe(res => {
      if (res && res.data) {
        this.tipList = res.data;
      }
    })
  }

  onNext() {
    this.router.navigate(['app/screening-disclosure/digital-signature']);
  }

}
