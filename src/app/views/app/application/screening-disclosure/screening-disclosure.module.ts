import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScreeningDisclosureRoutingModule } from './screening-disclosure-routing.module';
import { ScreeningDisclosureComponent } from './screening-disclosure.component';
import { TranslateModule } from '@ngx-translate/core';
import { PhotographTipsComponent } from './components/photograph-tips/photograph-tips.component';
import { DigitalSignatureComponent } from './components/digital-signature/digital-signature.component';
import { ProfileScreeningComponent } from './components/profile-screening/profile-screening.component';
import { ImageUploadModule } from 'src/app/views/shared/components/image-upload/image-upload.module';
import { AccordionModule } from 'ngx-bootstrap/accordion';


@NgModule({
  declarations: [
    ScreeningDisclosureComponent,
    PhotographTipsComponent,
    DigitalSignatureComponent,
    ProfileScreeningComponent],
  imports: [
    CommonModule,
    ScreeningDisclosureRoutingModule,
    TranslateModule.forChild(),
    ImageUploadModule,
    AccordionModule.forRoot(),
  ],
})
export class ScreeningDisclosureModule { }
