import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class ImageUploadComponent implements OnInit {
  field
  constructor() { }

  ngOnInit(): void {
  }

}
